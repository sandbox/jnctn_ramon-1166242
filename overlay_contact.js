/* jquery call to activate overlay */
$(document).ready(function() {
    if ($(".form-text").is(".error") || $(".form-textarea").is(".error")) {
        $(".overlayform-trigger[rel]").overlay({
            load:true
        })
    }else{
        $(".overlayform-trigger[rel]").overlay({
            onload: function(){
                $('.overlayname').focus();
            }
        });
    }
});

$("#block-overlay_contact-overlay_contact").bind("onLoad", function() {
    $('.overlayname').focus();
});
